var die1;
var die2;
var money = 50;
/*The below vars are for calculating the win rate and the degree of snark displayed*/
var win = 0;
var loss = 0;
var winRate;

function setMoney(){
    document.getElementById("money").innerHTML = money;
}
/*I was trying to figure out how to make a seperate "image changing" function and nest it
within the roll function, but it wasn't working for me, which resulted in this large,
most likely very inefficient code.*/
function roll(){
    var die1 = Math.floor((Math.random() * 6) + 1);
    var die2 = Math.floor((Math.random() * 6) + 1);
    switch(die1){
        case 1:
            document.getElementById("die1").src = "/_repos/kachurek/luckySevens/img/die1.png";
            break;
        case 2:
            document.getElementById("die1").src = "/_repos/kachurek/luckySevens/img/die2.png";
            break;
        case 3:
            document.getElementById("die1").src = "/_repos/kachurek/luckySevens/img/die3.png";
            break;
        case 4:
            document.getElementById("die1").src = "/_repos/kachurek/luckySevens/img/die4.png";
            break;
        case 5:
            document.getElementById("die1").src = "/_repos/kachurek/luckySevens/img/die5.png";
            break;
        case 6:
            document.getElementById("die1").src = "/_repos/kachurek/luckySevens/img/die6.png";
            break;
    }
    switch(die2){
        case 1:
            document.getElementById("die2").src = "/_repos/kachurek/luckySevens/img/die1.png";
            break;
        case 2:
            document.getElementById("die2").src = "/_repos/kachurek/luckySevens/img/die2.png";
            break;
        case 3:
            document.getElementById("die2").src = "/_repos/kachurek/luckySevens/img/die3.png";
            break;
        case 4:
            document.getElementById("die2").src = "/_repos/kachurek/luckySevens/img/die4.png";
            break;
        case 5:
            document.getElementById("die2").src = "/_repos/kachurek/luckySevens/img/die5.png";
            break;
        case 6:
            document.getElementById("die2").src = "/_repos/kachurek/luckySevens/img/die6.png";
            break;
    }
    var rollTotal = die1 + die2;
    if (rollTotal == 7){
        money += 4;
        win++;
    } else{
        money--;
        loss++;
    }
    var winRate = Math.floor((win / loss) * 100);
    document.getElementById("money").innerHTML = money;
    /*As noted in the HTML file, I had previously shown the win rate on the page, but now
    that I don't, the below line is unnecessary.
    document.getElementById("winRate").innerHTML = winRate;*/
    //Below is where I get to mock people for losing
    if (winRate <= 10){
        document.getElementById("header").innerHTML = "Unlucky Sevens";
    } else{
        document.getElementById("header").innerHTML = "Lucky Sevens";
    }
    
    if(winRate >= 75){
        document.getElementById("snark").innerHTML = "You are a master of fate, all shall tremble before your dice-related powers.";
    } else if(winRate >= 25){
        document.getElementById("snark").innerHTML = "Must be your lucky day.";
    } else if(winRate >= 10){
        document.getElementById("snark").innerHTML = "Maybe you shouldn't spend too much time at the casino.";
    } else if(winRate >= 0){
        document.getElementById("snark").innerHTML = "Sounds like someone has a case of the Mondays.";
    }
    
}
