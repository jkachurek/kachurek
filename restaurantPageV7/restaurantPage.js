function verifyContact(){
    var name = document.forms["contactForm"]["name"].value;
    if (name == null || name == ""){
        alert("Name must be filled in.");
        return false;
    }
    var email = document.forms["contactForm"]["email"].value;
    var phone = document.forms["contactForm"]["phone"].value;
    if ((email == null || email == "") && (phone == null || phone == "")){
        alert("At least one form of contact must be filled in (phone or email).");
        return false;
    }
}

function verifyReasons(){
    var reason = document.forms["contactForm"]["reasons"].value;
    var addInfo = document.forms["contactForm"]["addInfo"].value;
    if (reason == "other"){
        if (addInfo == ""){
            alert("Please provide a reason for inquiry.");
            return false;
        } else{
            return true;
        }
    }
}

function verifyDays(){
    if (
        document.getElementById("mon").checked == false &&
        document.getElementById("tue").checked == false &&
        document.getElementById("wed").checked == false &&
        document.getElementById("thu").checked == false &&
        document.getElementById("fri").checked == false) 
	{
		alert ('Please fill in at least one day of availability.');
		return false;
	}
}

function verify(){
    verifyContact();
    verifyReasons();
    verifyDays();
}
